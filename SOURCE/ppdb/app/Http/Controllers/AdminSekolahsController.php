<?php

namespace App\Http\Controllers;

use App\AdminSekolah;
use Illuminate\Http\Request;

class AdminSekolahsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('production.adminsekolah');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminSekolah  $adminSekolah
     * @return \Illuminate\Http\Response
     */
    public function show(AdminSekolah $adminSekolah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminSekolah  $adminSekolah
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminSekolah $adminSekolah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminSekolah  $adminSekolah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminSekolah $adminSekolah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminSekolah  $adminSekolah
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminSekolah $adminSekolah)
    {
        //
    }
}
