<?php

namespace App\Http\Controllers;

use App\MasterZona;
use Illuminate\Http\Request;

class MasterZonasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('production.masterZona');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterZona  $masterZona
     * @return \Illuminate\Http\Response
     */
    public function show(MasterZona $masterZona)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterZona  $masterZona
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterZona $masterZona)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterZona  $masterZona
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterZona $masterZona)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterZona  $masterZona
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterZona $masterZona)
    {
        //
    }
}
