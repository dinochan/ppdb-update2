<?php

namespace App\Http\Controllers;

use App\SKTM;
use Illuminate\Http\Request;

class SKTMsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SKTM  $sKTM
     * @return \Illuminate\Http\Response
     */
    public function show(SKTM $sKTM)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SKTM  $sKTM
     * @return \Illuminate\Http\Response
     */
    public function edit(SKTM $sKTM)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SKTM  $sKTM
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SKTM $sKTM)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SKTM  $sKTM
     * @return \Illuminate\Http\Response
     */
    public function destroy(SKTM $sKTM)
    {
        //
    }
}
