<?php

namespace App\Http\Controllers;

use App\WorkFlow;
use Illuminate\Http\Request;

class WorkFlowsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('production.workflow');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorkFlow  $workFlow
     * @return \Illuminate\Http\Response
     */
    public function show(WorkFlow $workFlow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorkFlow  $workFlow
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkFlow $workFlow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorkFlow  $workFlow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkFlow $workFlow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorkFlow  $workFlow
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkFlow $workFlow)
    {
        //
    }
}
