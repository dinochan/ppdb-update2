<?php

namespace App\Http\Controllers;

use App\MasterSKTM;
use Illuminate\Http\Request;

class MasterSKTMsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('production.masterSKTM');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterSKTM  $masterSKTM
     * @return \Illuminate\Http\Response
     */
    public function show(MasterSKTM $masterSKTM)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterSKTM  $masterSKTM
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterSKTM $masterSKTM)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterSKTM  $masterSKTM
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterSKTM $masterSKTM)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterSKTM  $masterSKTM
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterSKTM $masterSKTM)
    {
        //
    }
}
