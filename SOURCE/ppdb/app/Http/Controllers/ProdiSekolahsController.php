<?php

namespace App\Http\Controllers;

use App\ProdiSekolah;
use Illuminate\Http\Request;

class ProdiSekolahsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('production.prodisekolah');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProdiSekolah  $prodiSekolah
     * @return \Illuminate\Http\Response
     */
    public function show(ProdiSekolah $prodiSekolah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProdiSekolah  $prodiSekolah
     * @return \Illuminate\Http\Response
     */
    public function edit(ProdiSekolah $prodiSekolah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProdiSekolah  $prodiSekolah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProdiSekolah $prodiSekolah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProdiSekolah  $prodiSekolah
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProdiSekolah $prodiSekolah)
    {
        //
    }
}
