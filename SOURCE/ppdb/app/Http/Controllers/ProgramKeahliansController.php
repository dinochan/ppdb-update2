<?php

namespace App\Http\Controllers;

use App\ProgramKeahlian;
use Illuminate\Http\Request;

class ProgramKeahliansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('production.programkeahlian');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProgramKeahlian  $programKeahlian
     * @return \Illuminate\Http\Response
     */
    public function show(ProgramKeahlian $programKeahlian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProgramKeahlian  $programKeahlian
     * @return \Illuminate\Http\Response
     */
    public function edit(ProgramKeahlian $programKeahlian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProgramKeahlian  $programKeahlian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProgramKeahlian $programKeahlian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProgramKeahlian  $programKeahlian
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProgramKeahlian $programKeahlian)
    {
        //
    }
}
