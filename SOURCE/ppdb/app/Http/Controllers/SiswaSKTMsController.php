<?php

namespace App\Http\Controllers;

use App\SiswaSKTM;
use Illuminate\Http\Request;

class SiswaSKTMsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('production.siswaSKTM');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SiswaSKTM  $siswaSKTM
     * @return \Illuminate\Http\Response
     */
    public function show(SiswaSKTM $siswaSKTM)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SiswaSKTM  $siswaSKTM
     * @return \Illuminate\Http\Response
     */
    public function edit(SiswaSKTM $siswaSKTM)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SiswaSKTM  $siswaSKTM
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiswaSKTM $siswaSKTM)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SiswaSKTM  $siswaSKTM
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiswaSKTM $siswaSKTM)
    {
        //
    }
}
