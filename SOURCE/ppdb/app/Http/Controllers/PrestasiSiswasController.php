<?php

namespace App\Http\Controllers;

use App\PrestasiSiswa;
use Illuminate\Http\Request;

class PrestasiSiswasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('production.prestasiSiswa');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PrestasiSiswa  $prestasiSiswa
     * @return \Illuminate\Http\Response
     */
    public function show(PrestasiSiswa $prestasiSiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PrestasiSiswa  $prestasiSiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(PrestasiSiswa $prestasiSiswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PrestasiSiswa  $prestasiSiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PrestasiSiswa $prestasiSiswa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PrestasiSiswa  $prestasiSiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrestasiSiswa $prestasiSiswa)
    {
        //
    }
}
