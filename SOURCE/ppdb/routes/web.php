<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// ROUTING FOR HOME 
Route::get('/', 'HomesController@index');
Route::get('/home', 'HomesController@index');
// ROUTING FOR HOME 

// ROUTING FOR LOGIN
Route::get('/login', 'LoginsController@index');
// ROUTING FOR LOGIN

// ROUTIGN FOR SUPER ADMIN SEKOLAH
Route::get('/user', 'UsersController@index');
Route::get('/workflow', 'WorkFlowsController@index');
Route::get('/guard', 'GuardsController@index');
Route::get('/state', 'StatesController@index');
Route::get('/transition', 'TransitionsController@index');
Route::get('/zona', 'ZonasController@index');

// ROUTIGN FOR SUPER ADMIN SEKOLAH


// ROUTIGN FOR ADMIN SEKOLAH
Route::get('/adminsekolah', 'AdminSekolahsController@index');
Route::get('/akademik', 'AkademiksController@index');
Route::get('/prestasi', 'PrestasisController@index');
Route::get('/prestasiSiswa', 'PrestasiSiswasController@index');
Route::get('/kegiatan', 'KegiatansController@index');
Route::get('/masterSKTM', 'MasterSKTMsController@index');
Route::get('/masterZona', 'MasterZonasController@index');
Route::get('/nilai', 'NilaisController@index');
Route::get('/profile', 'ProfilesController@index');
Route::get('/orangtua', 'OrangtuasController@index');
Route::get('/pendaftaran', 'PendaftaransController@index');
Route::get('/prodisekolah', 'ProdiSekolahsController@index');
Route::get('/programkeahlian', 'ProgramKeahliansController@index');
Route::get('/sekolah', 'SekolahsController@index');
Route::get('/seleksi', 'SeleksisController@index');
Route::get('/siswa', 'SiswasController@index');
Route::get('/siswaSKTM', 'SiswaSKTMsController@index');
// ROUTIGN FOR ADMIN SEKOLAH

// ROUTING FOR DASHBOAR
Route::get('/admin', 'AdminsController@index');
Route::get('/admin2', 'AdminsController@index2');
Route::get('/admin3', 'AdminsController@index3');
// ROUTING FOR DASHBOAR


// ROUTING FORM 
Route::get('/form', 'FormsController@index');

// ROUTING FORM 
