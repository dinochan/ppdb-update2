@include('layout/main')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Add Orang Tua</h3>
      </div>
      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
        </div>
      </div>
    </div>

    

    <!-- form color picker -->
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Add Orang Tua</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
              </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <br />
          <form class="form-horizontal form-label-left">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Siswa</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="input-group demo2">
                  <input type="text" placeholder="nama siswa" class="form-control" />
                  <span class="input-group-addon"><i></i></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Telepon</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="input-group demo2">
                  <input type="text" placeholder="Nomor Telepon" class="form-control" />
                  <span class="input-group-addon"><i></i></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Ayah</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="input-group demo2">
                  <input type="text" placeholder="Nama Ayah" class="form-control" />
                  <span class="input-group-addon"><i></i></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Ibu</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="input-group demo2">
                  <input type="text" placeholder="Nama Ibu" class="form-control" />
                  <span class="input-group-addon"><i></i></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Pendidikan Ayah</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="input-group demo2">
                  <input type="text" placeholder="Pendidikan Ayah" class="form-control" />
                  <span class="input-group-addon"><i></i></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Pekerjaan Ayah</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="input-group demo2">
                  <input type="text" placeholder="Pekerjaan Ayah" class="form-control" />
                  <span class="input-group-addon"><i></i></span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Pendidikan Ibu</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="input-group demo2">
                  <input type="text" placeholder="Pendidikan Ibu" class="form-control" />
                  <span class="input-group-addon"><i></i></span>
                </div>
              </div>
            </div>
             <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Pekerjaan Ibu</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="input-group demo2">
                  <input type="text" placeholder="Pekerjaan Ibu" class="form-control" />
                  <span class="input-group-addon"><i></i></span>
                </div>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btn-primary" type="button">Cancel</button>
                <button class="btn btn-primary" type="reset">Reset</button>
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /form color picker -->


    <!-- table -->
    @include('layout/table')

    <!-- table -->
  </div>





</div>
<!-- /page content -->
@include('layout/foot')
