 <style type="text/css">
 th{
  text-align: center;
}
td{
  text-align: center;
}

</style>

<!-- launch modal -->
<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail data ID</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <tr>
         <td>Tiger Nixon</td>
         <td>System Architect</td>
         <td>Edinburgh</td>
         <td>61</td>
         <td>2011/04/25</td>
       </tr>
       <tr>
         <td>Tiger Nixon</td>
         <td>System Architect</td>
         <td>Edinburgh</td>
         <td>61</td>
         <td>2011/04/25</td>
       </tr>
       <tr>
         <td>Tiger Nixon</td>
         <td>System Architect</td>
         <td>Edinburgh</td>
         <td>61</td>
         <td>2011/04/25</td>
       </tr>
     </div>
     <div class="modal-footer">
      <button type="button" onclick="add()" class="btn btn-danger" data-dismiss="modal">Close</button>

    </div>
  </div>
</div>
</div>
<!-- launch modal -->

<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Detail Table</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <button class="btn btn-warning"><i class="fa fa-plus"></i> Tambah</button>
      <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
      <button class="btn btn-primary"><i class="fa fa-download"></i> PDF</button>
      <button class="btn btn-success"><i class="fa fa-download"></i> Excel</button>
      <br><br><br>
      <table id="datatable-checkbox" class="table table-striped jambo_table bulk_action ">
        <thead>
          <tr>
           <th>
             <th style="text-align: left;">
              <input type="checkbox" id="check-all" class="flat">
            </th>
          </th>
          <th>Name</th>
          <th>Position</th>
          <th>Office</th>
          <th>Age</th>
          <th>Start date</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
           <th style="text-align: left;"><input type="checkbox" id="check-all" class="flat"></th>
         </td>
         <td>Tiger Nixon</td>
         <td>System Architect</td>
         <td>Edinburgh</td>
         <td>61</td>
         <td>2011/04/25</td>
         <td colspan="3" >

           <a class="btn btn-success" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-eye"></i></a>
           <a class="btn btn-primary"  href="#"><i class="fa fa-upload"></i></a>
           <button class="btn btn-danger" onclick="sweet()"><i class="fa fa-trash"></i></button>
         </td>
       </tr>
       <tr>
        <td>
         <th style="text-align: left;"><input type="checkbox" id="check-all" class="flat"></th>
       </td>
       <td>Tiger Nixon</td>
       <td>System Architect</td>
       <td>Edinburgh</td>
       <td>61</td>
       <td>2011/04/25</td>
       <td colspan="3" >
        <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
        <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
        <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
      </td>
    </tr>
    <tr>
      <td>
       <th style="text-align: left;"><input type="checkbox" id="check-all" class="flat"></th>
     </td>
     <td>Tiger Nixon</td>
     <td>System Architect</td>
     <td>Edinburgh</td>
     <td>61</td>
     <td>2011/04/25</td>
     <td colspan="3" >
      <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
      <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
      <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
    </td>
  </tr>
  <tr>
    <td>
     <th style="text-align: left;"><input type="checkbox" id="check-all" class="flat"></th>
   </td>
   <td>Tiger Nixon</td>
   <td>System Architect</td>
   <td>Edinburgh</td>
   <td>61</td>
   <td>2011/04/25</td>
   <td colspan="3" >
    <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
    <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
    <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
  </td>
</tr>
<tr>
  <td>
   <th style="text-align: left;"><input type="checkbox" id="check-all" class="flat"></th>
 </td>
 <td>Tiger Nixon</td>
 <td>System Architect</td>
 <td>Edinburgh</td>
 <td>61</td>
 <td>2011/04/25</td>
 <td colspan="3" >
  <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
  <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
  <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
</td>
</tr>
<tr>
  <td>
   <th style="text-align: left;"><input type="checkbox" id="check-all" class="flat"></th>
 </td>
 <td>Tiger Nixon</td>
 <td>System Architect</td>
 <td>Edinburgh</td>
 <td>61</td>
 <td>2011/04/25</td>
 <td colspan="3" >
  <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
  <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
  <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
</td>
</tr>
<tr>
  <td>
   <th style="text-align: left;"><input type="checkbox" id="check-all" class="flat"></th>
 </td>
 <td>Tiger Nixon</td>
 <td>System Architect</td>
 <td>Edinburgh</td>
 <td>61</td>
 <td>2011/04/25</td>
 <td colspan="3" >
  <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
  <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
  <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
</td>
</tr>
<tr>
  <td>
   <th style="text-align: left;"><input type="checkbox" id="check-all" class="flat"></th>
 </td>
 <td>Tiger Nixon</td>
 <td>System Architect</td>
 <td>Edinburgh</td>
 <td>61</td>
 <td>2011/04/25</td>
 <td colspan="3" >
  <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
  <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
  <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
</td>
</tr>
<tr>
  <td>
   <th style="text-align: left;"><input type="checkbox" id="check-all" class="flat"></th>
 </td>
 <td>Tiger Nixon</td>
 <td>System Architect</td>
 <td>Edinburgh</td>
 <td>61</td>
 <td>2011/04/25</td>
 <td colspan="3" >
  <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
  <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
  <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
</td>
</tr>
<tr>
  <td>
   <th style="text-align: left;"><input type="checkbox" id="check-all" class="flat"></th>
 </td>
 <td>Tiger Nixon</td>
 <td>System Architect</td>
 <td>Edinburgh</td>
 <td>61</td>
 <td>2011/04/25</td>
 <td colspan="3" >
  <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
  <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
  <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
</td>
</tr>
<tr>
  <td>
   <th style="text-align: left;"><input type="checkbox" id="check-all" class="flat"></th>
 </td>
 <td>Tiger Nixon</td>
 <td>System Architect</td>
 <td>Edinburgh</td>
 <td>61</td>
 <td>2011/04/25</td>
 <td colspan="3" >
  <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
  <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
  <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
</td>
</tr>

</tbody>
</table>
</div>
</div>
</div>


<div class="clearfix"></div>
