 <style type="text/css">
 th{
  text-align: center;
}
td{
  text-align: center;

}

</style>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Detail Table</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>

    <div class="x_content">
      <div class="row no-print">
        <div class="col-xs-12">
         <button class="btn btn-warning" onclick="window.print();"><i class="fa fa-plus"></i> Tambah</button>
         <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
         <button class="btn btn-primary"><i class="fa fa-download"></i> PDF</button>
         <button class="btn btn-success"><i class="fa fa-download"></i> Excel</button>
         <div class="col-md-3 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Cari Data...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </div>
      </div>
      
    </div><br>
    <div class="table-responsive">
      <table id="datatable" class="table table-striped jambo_table bulk_action">
        <thead>
          <tr class="headings">
            <th>
              <input type="checkbox" id="check-all" class="flat">
            </th>
            <th class="column-title">Invoice </th>
            <th class="column-title">Invoice Date </th>
            <th class="column-title">Order </th>
            <th class="column-title">Bill to Name </th>

            <th class="column-title no-link last"><span class="nobr">Action</span>
            </th>
            <th class="bulk-actions" colspan="7">
              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
            </th>
          </tr>
        </thead>

        <tbody>
          <tr class="even pointer">
            <td class="a-center ">
              <input type="checkbox" class="flat" name="table_records">
            </td>
            <td class=" ">121000040</td>
            <td class=" ">May 23, 2014 11:47:56 PM </td>
            <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
            <td class=" ">John Blank L</td>

            <td colspan="3" >
              <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
              <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
              <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <tr class="even pointer">
            <td class="a-center ">
              <input type="checkbox" class="flat" name="table_records">
            </td>
            <td class=" ">121000040</td>
            <td class=" ">May 23, 2014 11:47:56 PM </td>
            <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
            <td class=" ">John Blank L</td>

            <td colspan="3" >
              <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
              <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
              <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <tr class="even pointer">
            <td class="a-center ">
              <input type="checkbox" class="flat" name="table_records">
            </td>
            <td class=" ">121000040</td>
            <td class=" ">May 23, 2014 11:47:56 PM </td>
            <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
            <td class=" ">John Blank L</td>

            <td colspan="3" >
              <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
              <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
              <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <tr class="even pointer">
            <td class="a-center ">
              <input type="checkbox" class="flat" name="table_records">
            </td>
            <td class=" ">121000040</td>
            <td class=" ">May 23, 2014 11:47:56 PM </td>
            <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
            <td class=" ">John Blank L</td>

            <td colspan="3" >
              <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
              <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
              <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <tr class="even pointer">
            <td class="a-center ">
              <input type="checkbox" class="flat" name="table_records">
            </td>
            <td class=" ">121000040</td>
            <td class=" ">May 23, 2014 11:47:56 PM </td>
            <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
            <td class=" ">John Blank L</td>

            <td colspan="3" >
              <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
              <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
              <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
          <tr class="even pointer">
            <td class="a-center ">
              <input type="checkbox" class="flat" name="table_records">
            </td>
            <td class=" ">121000040</td>
            <td class=" ">May 23, 2014 11:47:56 PM </td>
            <td class=" ">121000210 <i class="success fa fa-long-arrow-up"></i></td>
            <td class=" ">John Blank L</td>

            <td colspan="3" >
              <a class="btn btn-success" href="#"><i class="fa fa-eye"></i></a>
              <a class="btn btn-primary" href="#"><i class="fa fa-upload"></i></a>
              <a class="btn btn-danger" href="#"><i class="fa fa-trash"></i></a>
            </td>
          </tr>

        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<div class="clearfix"></div>
